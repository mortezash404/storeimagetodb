﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoStoreWebApp.Data;
using PhotoStoreWebApp.Models;

namespace PhotoStoreWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private readonly AppDbContext _context;

        public PhotoController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var photo = await _context.Photos.FindAsync(id);

            var imageBase64Data = Convert.ToBase64String(photo.PhotoData);

            return Ok(imageBase64Data);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] IFormFile file)
        {
            var photo = new Photo
            {
                Name = file.FileName,
                Size = file.Length
            };

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);

                photo.PhotoData = ms.ToArray();
            }

            _context.Add(photo);

            await _context.SaveChangesAsync();

            return Ok(photo);
        }
    }
}