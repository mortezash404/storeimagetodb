﻿using Microsoft.EntityFrameworkCore;
using PhotoStoreWebApp.Models;

namespace PhotoStoreWebApp.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<Photo> Photos { get; set; }
        
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}
